var express = require("express");
var bodyParser = require("body-parser");
var records = require("./records/records.js");
var gameapps = require("./gameapps.js");
var app = express();
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

//app.route = '/records';
app.get('/records', function (req, res) {
  	gameapps.getAll(req, res);
});
app.get('/records/:app', function (req, res) {
  	records.get(req.params.app, [], req, res);
});

/*app.post('/records/:app/:ID/:puntuacio', function (req, res) {
  	insert(req.params.app, req.params.ID, req.params.puntuacio, req, res);
});*/

app.post('/records/:app', function (req, res) {
	//console.log(req.body);
  	records.insert(req.params.app, req.body.ID, req.body.puntuacio, [], req, res);
});


app.listen(process.env.PORT || 8000, function () {
  	console.log('Example app listening on port 8000!');
});