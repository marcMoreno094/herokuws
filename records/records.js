//var json = JSON.parse(require('fs').readFileSync("records/records.json", 'utf8'));

/*var json = {
"AppX":[
		{"ID":"player1", "puntuacio":100},
		{"ID":"player2", "puntuacio":200},
		{"ID":"player3", "puntuacio":300},
		{"ID":"player4", "puntuacio":400},
		{"ID":"player5", "puntuacio":500},
		{"ID":"player6", "puntuacio":600},
	],
	"AppY":[
		{"ID":"player1", "puntuacio":100},
		{"ID":"player2", "puntuacio":200},
		{"ID":"player3", "puntuacio":300},
		{"ID":"player4", "puntuacio":400},
		{"ID":"player5", "puntuacio":500},
		{"ID":"player6", "puntuacio":600},
		{"ID":"player7", "puntuacio":700},
		{"ID":"player8", "puntuacio":800},
	]
};



exports.listAll = function(req, res){
	res.json(json);
}

exports.list = function(app, req, res){
	if (json.hasOwnProperty(app)){
		res.json(json[app]);
	}
}

exports.insert = function(app, user, points, req, res){
	if (json.hasOwnProperty(app)){
		//console.log({"ID":user, "puntuacio":points}, json, app);
		json[app].push({"ID":user, "puntuacio":points});
		console.log("Success!");

		res.json(json);
	}else {
		res.send("Error!");
		console.log("Error!");
	}
}*/


var db = require('./db.js');

exports.get = function(app, params, req, res){
	//console.log('Query a '+app+'');
	db.query('SELECT * from "RECORDS" WHERE app = $1', [app], function(err, result){
		if (!err){
			res.status(200);
			res.json(result.rows);
		}else res.status(404);
	});
};

exports.insert = function(app, id, puntuacio, params, req, res){
	if (!id || !puntuacio){
		res.status(422);
		res.json({err: 'A3'});
		return console.error(err);
	}
	console.log("Atenent una POST request\n");
	db.query('INSERT INTO "RECORDS" (player, app, score) VALUES ($2, $1, '+puntuacio+');', [app, id], function(err, result){
		console.log("Result de POST:\n");
		if (!err){
			console.log("SUCCESS!\n");
			res.status(200);
			res.json({	Success : true });
		}else{
			console.log("ERROR!\n");
			res.status(500);
			res.json({err: 'A4'});
		}
	});
};